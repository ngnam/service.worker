using System.ComponentModel.DataAnnotations;

namespace ServiceWorker.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}