﻿using System.Threading.Tasks;
using ServiceWorker.Configuration.Dto;

namespace ServiceWorker.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
