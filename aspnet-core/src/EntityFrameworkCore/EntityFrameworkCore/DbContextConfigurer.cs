using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace ServiceWorker.EntityFrameworkCore
{
    public static class DbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<DbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<DbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
