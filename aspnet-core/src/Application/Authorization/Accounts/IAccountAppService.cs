﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ServiceWorker.Authorization.Accounts.Dto;

namespace ServiceWorker.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
