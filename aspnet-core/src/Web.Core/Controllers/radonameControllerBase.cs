using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace ServiceWorker.Controllers
{
    public abstract class radonameControllerBase: AbpController
    {
        protected radonameControllerBase()
        {
            LocalizationSourceName = Consts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
