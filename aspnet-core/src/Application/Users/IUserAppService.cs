using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ServiceWorker.Roles.Dto;
using ServiceWorker.Users.Dto;

namespace ServiceWorker.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
