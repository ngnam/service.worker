﻿namespace ServiceWorker
{
    public class Consts
    {
        public const string LocalizationSourceName = "randoname";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
