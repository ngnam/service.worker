using Microsoft.AspNetCore.Antiforgery;
using ServiceWorker.Controllers;

namespace ServiceWorker.Web.Host.Controllers
{
    public class AntiForgeryController : radonameControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
