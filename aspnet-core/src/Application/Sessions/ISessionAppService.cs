﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ServiceWorker.Sessions.Dto;

namespace ServiceWorker.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
