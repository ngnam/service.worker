import { radonameTemplatePage } from './app.po';

describe('radoname App', function() {
  let page: radonameTemplatePage;

  beforeEach(() => {
    page = new radonameTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
