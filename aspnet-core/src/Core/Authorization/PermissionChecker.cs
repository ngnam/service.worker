﻿using Abp.Authorization;
using ServiceWorker.Authorization.Roles;
using ServiceWorker.Authorization.Users;

namespace ServiceWorker.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
