﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using ServiceWorker.Authorization.Roles;
using ServiceWorker.Authorization.Users;
using ServiceWorker.MultiTenancy;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ServiceWorker.EntityFrameworkCore
{
    public class DbContext : AbpZeroDbContext<Tenant, Role, User, DbContext>
    {
        /* Define a DbSet for each entity of the application */
        //[Serializable]
        //[Table("AbpLanguages")]
        //public class ApplicationLanguage : FullAuditedEntity, IMayHaveTenant
        //{
        //    public int? TenantId { get; set; }
        //    public string Name { get; set; }
        //}

        //[Serializable]
        //[Table("AbpLanguageTexts")]
        //public class ApplicationLanguageText : AuditedEntity<long>, IMayHaveTenant
        //{
        //    public int? TenantId { get; set; }
        //}

        //private void ConfigureApplicationLanguage(EntityTypeBuilder<ApplicationLanguage> entity)
        //{
        //    entity.ToTable("AbpLanguages");

        //    entity.Property(e => e.Id)
        //        .HasColumnName("id")
        //        .HasColumnType("int(11)");
        //}

        //private void ConfigureApplicationLanguageText(EntityTypeBuilder<ApplicationLanguageText> entity)
        //{
        //    entity.ToTable("AbpLanguageTexts");

        //    entity.Property(e => e.Id)
        //        .HasColumnName("id")
        //        .HasColumnType("int(11)");
        //}

        //public virtual DbSet<ApplicationLanguage> AbpLanguages { get; set; }
        //public virtual DbSet<ApplicationLanguageText> AbpLanguageTexts { get; set; }

        public DbContext(DbContextOptions<DbContext> options)
            : base(options)
        {
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<ApplicationLanguage>(ConfigureApplicationLanguage);
        //    modelBuilder.Entity<ApplicationLanguageText>(ConfigureApplicationLanguageText);
        //}

    }
}
