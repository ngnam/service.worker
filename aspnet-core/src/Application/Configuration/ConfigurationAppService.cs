﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using ServiceWorker.Configuration.Dto;

namespace ServiceWorker.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : AppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
